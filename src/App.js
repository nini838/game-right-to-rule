import './App.css';
import React from 'react';
import MainSection from './components/MainSection/MainSection';

const App = (props) => {
  return (
    <main className="JoJo">
      <MainSection state={props.state}/>
    </main>
  );
}

export default App;
