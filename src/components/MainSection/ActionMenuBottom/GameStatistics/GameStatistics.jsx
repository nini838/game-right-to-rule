import React from 'react';
import Typography from '@material-ui/core/Typography';
import PeopleIcon from '@material-ui/icons/People';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import HelpIcon from '@material-ui/icons/Help';
import { ReactComponent as Coins } from '../../../../assets/svg/coins.svg';
// import { Coins } from '../../../../assets/svg';
import css from './GameStatistics.module.scss';

const GameStatistics = (props) => {
  const population = '40';
  const populationUnits = 'mln';

  const income = 476;
  const incomeUnits = 'bil';
  const expenses = 509;
  const expensesUnits = 'bil';
  const budgetCurrency = '₴';

  const happiness = 995;

  const Fall = () => <ExpandMoreIcon color="error" style={{ width: 20, height: 20 }} />;
  const Rise = () => <ExpandMoreIcon style={{ color: "green", transform: "rotate(180deg)", width: 20, height: 20 }} />;

  return (
    <div className={css.GameStatistics}>
      <div className="row">
        <PeopleIcon className={css.svg} />
        <Typography className={css.value} >
          {population} {populationUnits} <Fall />
        </Typography>
      </div>

      <div className="row">
        <Coins className={css.svg} />
        <Typography className={css.value}>
          {income} {incomeUnits} {budgetCurrency} 
          <Fall /> 
          /&nbsp;
          {expenses} {expensesUnits} {budgetCurrency} 
          <Rise />
        </Typography>
      </div>

      <div className="row">
        <EmojiEmotionsIcon className={css.svg} />
        <Typography className={css.value} >
          {happiness} % <Rise />
        </Typography>
      </div>

      <div className="row">
        <HelpIcon className={css.svg} />
        <Typography className={css.value} >
          ? ? ?
        </Typography>
      </div>
    </div>
  )
}

export default GameStatistics;
