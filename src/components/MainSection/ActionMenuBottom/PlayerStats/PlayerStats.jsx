import React from 'react';
import { connect } from 'react-redux';
import WatchIcon from '@material-ui/icons/WatchLater';
import BoltIcon from '@material-ui/icons/OfflineBolt';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import LinearProgress from '@material-ui/core/LinearProgress';

import { MAX_STAMINA, MAX_ACTION_POINTS, MAX_DEPRESSION } from '../../../../constants/constants';
import css from './PlayerStats.module.scss';

const PlayerStats = (props) => {
  const { actionPoints, stamina, depression, day } = props;

  return (
    <div className={css.PlayingTime}>
      Day {day}

      <div className="row">
        <WatchIcon /> { actionPoints +'/'+ MAX_ACTION_POINTS }
        <LinearProgress className={css.progress} variant="determinate" value={actionPoints} />
      </div>
      <div className="row">
        <BoltIcon /> { stamina +'/'+ MAX_STAMINA }
        <LinearProgress className={css.progress} variant="determinate" value={stamina} />
      </div>
      <div className="row">
        <EmojiEmotionsIcon /> { depression +'/'+ MAX_DEPRESSION }
        <LinearProgress className={css.progress} variant="determinate" value={depression} />
      </div>
    </div>
  )
}

const mapStateToProps = (state) => ({
  actionPoints: state.player.actionPoints,
  stamina: state.player.stamina,
  depression: state.player.depression,
  day: state.time.day,
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerStats);
