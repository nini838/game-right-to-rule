import React from 'react';
import InternalPolicy from './InternalPolicy';
import e1 from './EventTasks.module.scss';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const tabList = [
  {
    label: "InternalPolicy",
    icon: <PhoneIcon />,
    content: <InternalPolicy />,
  },
  {
    label: "IllegalAction",
    icon: <FavoriteIcon />,
  },
  {
    label: "PersonPinIcon",
    icon: <PersonPinIcon />,
  },
];

const EventTasks = (props) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={e1.EventTasks}>
      <AppBar position="static" color="default">
        <Tabs value={value} onChange={handleChange} >
          { tabList.map(i => (
            <Tab label={i.label} icon={i.icon} key={i.label} />
          ))}
          {/* <Tab label="InternalPolicy" icon={<PhoneIcon />} />
          <Tab label="IllegalAction" icon={<FavoriteIcon />} />
          <Tab label="ForeignPolicy" icon={<PersonPinIcon />} /> */}
        </Tabs>
      </AppBar>
      { tabList.map((i, index) => (
        // <div hidden={value !== index} key={i.label} >
          
        // </div>
        value === index && i.content
      ))}


    </div>
  );
}

export default EventTasks;
