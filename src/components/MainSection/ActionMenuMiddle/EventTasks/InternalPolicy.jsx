import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

// import budgPic from '../../../../assets/pic/9_main.jpeg';
import budgPic from '../../../../assets/pic/Black+money+hole.jpg';
import bussinesPic from '../../../../assets/pic/companies.jpg';
import ecologyPic from '../../../../assets/pic/ecology.jpg';
import taxesPic from '../../../../assets/pic/taxes.jpg';
import crimePic from '../../../../assets/pic/crime.jpg';
import lawsPic from '../../../../assets/pic/laws.jpg';
import css from './EventTasks.module.scss';

const cardList = [
  {
    title: 'Budget',
    pic: budgPic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },
  {
    title: 'Bussines Analinics',
    pic: bussinesPic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },
  {
    title: 'Ecology',
    pic: ecologyPic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },
  {
    title: 'Taxes',
    pic: taxesPic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },
  {
    title: 'Crime',
    pic: crimePic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },
  {
    title: 'Laws',
    pic: lawsPic,
    descriprion: `Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
    across all continents except Antarctica`,
  },

]
const InternalPolicy = (props) => {

  return (
    <div className={css.InternalPolicy}>
        {/* <Button variant="outlined">Default action</Button>
        <Button variant="outlined" color="primary">
            Important action 
        </Button>
        <Button variant="outlined" color="secondary">
            Bad action
        </Button>
        <Button variant="outlined" disabled>
            Disabled action
        </Button> */}
      { 
        cardList.map(card =>       
          <Card className={css.card} key={card.title}>
            <CardActionArea>
              <CardMedia
                className={css.pic}
                image={card.pic}
                title={card.title}
              />
              <CardContent className={css.cardContent}>
                <Typography gutterBottom variant="h5">{card.title}</Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {card.descriprion}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        )
      }

    </div>
  );

}

export default InternalPolicy;
