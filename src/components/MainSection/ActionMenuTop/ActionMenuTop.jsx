import React from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


import GlobalMenu from './GlobalMenu/GlobalMenu';
import AppTitle from './AppTitle/AppTitle';
import MainSearch from './MainSearch/MainSearch';
import MainNews from './MainNews/MainNews';
import Notifications from './Notifications/Notifications';
import css from './ActionMenuTop.module.scss';

const ActionMenuTop = (props) => {
  console.log('!!!  in ACtion menu', props.stMnTop);

  return (
    <AppBar position="static" className={css.ActionMenuTop}>
      <Toolbar>
        <GlobalMenu/>
        <AppTitle />
        <MainSearch />
        <MainNews/>
        <div className={css.grow} />
        <Notifications />
      </Toolbar>
    </AppBar>
  )
}

const mapStateToProps = (state) => ({
  stMnTop: state.stateMenuTop,
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionMenuTop);
