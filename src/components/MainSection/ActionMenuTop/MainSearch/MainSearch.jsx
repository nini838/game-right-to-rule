import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';

import css from './MainNews.module.scss';

const MainSearch = (props) => {
  return (
    <div className={css.search}>
      <SearchIcon />
      <InputBase
        placeholder="Search…"
        inputProps={{ 'aria-label': 'search' }}
      />
    </div>
  )
}

export default MainSearch;
