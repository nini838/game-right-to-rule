import React from 'react';
import { connect } from 'react-redux';

import ActionMenuTop from './ActionMenuTop/ActionMenuTop';
import ActionMenuMiddle from './ActionMenuMiddle/ActionMenuMiddle';
import ActionMenuBottom from './ActionMenuBottom/ActionMenuBottom';
import m1 from './MainSection.module.css';

const MainSection = (props) => {
  console.log('!!!  ', props.stMnTop);

  return (
    <div className={m1.MainSection}>
      <ActionMenuTop/>
      <ActionMenuMiddle/>
      <ActionMenuBottom/>
    </div>
  )
}

const mapStateToProps = (state) => ({
  stMnTop: state.stateMenuTop,
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainSection);
